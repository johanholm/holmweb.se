import VueRouter from 'vue-router';

let routes = [
    {
        path: '/',
        name: 'home',
        component: require('./views/Home.vue')
    },
    {
        path: '/about',
        name: 'about',
        component: require('./views/About.vue')
    },
    {
        path: '/portfolio',
        name: 'portfolio',
        component: require('./views/Portfolio.vue')
    },
    {
        path: '/contact',
        name: 'contact',
        component: require('./views/Contact.vue')
    }
];

export default new VueRouter({
    routes,
    linkActiveClass: 'is-active'
});