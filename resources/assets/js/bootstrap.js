import Vue from 'vue';
import VueRouter from 'vue-router';
import axios from 'axios';

global.Vue = Vue;
global.Bus = new Vue();
global.axios = axios;

Vue.use(VueRouter);

global.axios.defaults.headers.common = {
    'X-Requested-With': 'XMLHttpRequest'
};