export default class FallingStars {
    constructor(canvas) {
        this.canvas = canvas;
        this.ctx = canvas.getContext("2d");
        this.W = 1500;
        this.H = 1500;
        this.particles = [];

        this.setUpStars(50)
            .then(() => {
                // Draw the falling stars
                setInterval(() => {
                    this.draw();
                }, 50);
            });
    }

    setUpStars(count) {
        return new Promise((accept, reject) => {
            for(let i = 1; i <= count; i++)
            {
                this.particles.push(new StarParticle(this.W, this.H));
                if(i === count) {
                    accept();
                }
            }
        });

    }

    draw() {
        //Moving this BG paint code insde draw() will help remove the trail
        //of the particle
        //Lets paint the canvas black
        //But the BG paint shouldn't blend with the previous frame
        this.ctx.globalCompositeOperation = "source-over";
        //Lets reduce the opacity of the BG paint to give the final touch
        this.ctx.fillStyle = "rgba(0, 0, 0, 0.3)";
        this.ctx.fillRect(0, 0, this.W, this.H);

        //Lets blend the particle with the BG
        this.ctx.globalCompositeOperation = "lighter";

        //Lets draw particles from the array now
        for(let t = 0; t < this.particles.length; t++)
        {
            let p = this.particles[t];

            this.ctx.beginPath();

            //Time for some colors
            let gradient = this.ctx.createRadialGradient(p.x, p.y, 0, p.x, p.y, p.radius);
            gradient.addColorStop(0, "white");
            gradient.addColorStop(0.4, "white");
            gradient.addColorStop(0.4, p.color);
            gradient.addColorStop(1, "black");

            this.ctx.fillStyle = gradient;
            this.ctx.arc(p.x, p.y, p.radius, Math.PI*2, 0);
            this.ctx.fill();

            //Lets use the velocity now
            p.x += p.vx;
            p.y += p.vy;

            //To prevent the balls from moving out of the canvas
            if(p.x < -50) p.x = this.W+50;
            if(p.y < -50) p.y = this.H+50;
            if(p.x > this.W+50) p.x = -50;
            if(p.y > this.H+50) p.y = -50;
        }
    }
}

class StarParticle {
    constructor(W, H) {
        // Random position on the canvas
        this.x = Math.random()*W;
        this.y = Math.random()*H;

        // Lets add velocity to each particle to replicate the falling effect
        this.vx = -20;
        this.vy = 20;

        // Colors
        let r = 255;
        let g = 255;
        let b = 255;
        this.color = "rgba("+r+", "+g+", "+b+", 0.5)";

        // Random size
        this.radius = Math.floor(Math.random() * 3) + 1;
    }
}