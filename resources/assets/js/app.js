import './bootstrap';
import router from './router';

Vue.component('app-intro', require('./components/AppIntro.vue'));
Vue.component('app-header', require('./components/AppHeader.vue'));

router.afterEach((to, from) => {
    Bus.$emit('page.changed', to);
});

new Vue({
    el: '#app',
    router,
    mounted() {
        this.pageReady = true;
        Bus.$emit('page.ready');
    },
    data: {
        pageReady: false,
    },
});